package basebot.ceespring.mmxvii;

import basebot.ceespring.*;
import basebot.ceespring.objects.CEESTableLine;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import basebot.ceespring.objects.CEESUser;

import javax.security.auth.login.FailedLoginException;
import org.wikipedia.BaseBot;
import org.wikipedia.Wiki;
import objects.Credentials;

/**
 *
 * @author Base <base-w at yandex.ru>
 */
public class StatisticsCollectorVII {

    static int maincounter = 0;
    static int maincounterlim = 0;
    static Set<CEESTableLine> rows = new HashSet<>();
    final static Object LOCK = new Object();
    static Set<String> theLists = new HashSet<>();
    static Set<String> qs = new HashSet<>(); // Wikibase entities
    static Map<String, Map<String, CEESUser>> wikisusers = new HashMap<>();

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     * @throws javax.security.auth.login.FailedLoginException
     * @throws java.lang.InterruptedException
     */
    public static void main(String[] args) throws IOException, FailedLoginException, InterruptedException {
        final Credentials c = new Credentials(args);
        BaseBot d = initialiseWiki("www.wikidata.org", c);
        // @todo investigate if I can somehow use single login for all wikis
        // SUL was made for something, wasn't it?
        BaseBot m = initialiseWiki("meta.wikimedia.org", c);

        fetchListedArticlesEntities(m, d);
        Map<String, String> participatingWikis = fetchTemplateUsingWikis(d);

        //Adding fancy wikis here for simplicity.
        List<String> wikiDatabases = new ArrayList<>(participatingWikis.keySet());
        // wikiDatabases.addAll(Arrays.asList("cswiki", "dewiki")); // if needed

        maincounterlim = wikiDatabases.size();
        System.out.println("maincounterlim = " + maincounterlim);
        ExecutorService service = Executors.newCachedThreadPool();
        final Map<String, Map<String, String>> topics = Config.populateTopics(d);
        final Map<String, Map<String, String>> countries = Config.populateCountries(d);

        for (int i = 0; i < wikiDatabases.size(); i++) {
            final String key = wikiDatabases.get(i);
            final int threadcounter = i;
            if (!key.matches("[_a-z]+wiki")) {
                maincounter++;
                continue;
            }

            final String wikiName = key.substring(0, key.indexOf("wiki")).replace("_", "-").replace("be-x-old", "be-tarask")
                    + ".wikipedia.org";
            final String tlname = participatingWikis.getOrDefault(key, "");

            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    Thread.currentThread().setName(wikiName);
                    try {
                        workInWiki(threadcounter, wikiName, tlname, key, d, countries, topics, c);
                    } catch (Exception ex) {
                        Logger.getLogger(StatisticsCollectorVII.class.getName()).log(Level.SEVERE, null, ex);
                        System.exit(666);
                    }
                }
            };
            Thread.sleep(5000);
            Future<?> submit = service.submit(runnable);
        }
    }

    /**
     * Creates a BaseBot object of a wiki given, logs in there and sets up mark
     * bot and minor booleans
     *
     * @param wiki
     * @param c
     * @return
     * @throws IOException
     * @throws FailedLoginException
     */
    public static BaseBot initialiseWiki(String wiki, Credentials c)
            throws IOException, FailedLoginException {
        BaseBot w = new BaseBot(wiki);
        w.setUserAgent("BaseBot");
        w.login(c.wikilogin, c.wikipassword);
        w.setMarkBot(true);
        w.setMarkMinor(true);
        return w;
    }

    public static void workInWiki(
            int thread,
            String wikiName,
            String tlname,
            String wdwiki,
            BaseBot d,
            Map<String, Map<String, String>> countries,
            Map<String, Map<String, String>> topics,
            Credentials c
    ) throws Exception {
        System.out.println(wikiName + "\t" + tlname + "\t" + Thread.currentThread().getName());

        BaseBot w = initialiseWiki(wikiName, c);
        String[] talkPages;

        // true for wikis with the source of data other than the template
        // it makes no sense to try to parse talkpage for them
        boolean templatePhobic = false;
        Map<String, String> templatePhobicArticles = new HashMap<>();
        switch (wikiName) {
//            case "de.wikipedia.org":
            default:
                talkPages = w.whatTranscludesHere(tlname, 1);
                break;
        }

        boolean amIBot = w.getUser(c.wikilogin).isA("bot");
        int batchlim = amIBot ? 450 : 45;

        Map<String, String> pageTexts = fetchPageTexts(batchlim, w, talkPages);

        //TOPICS
        Set<String> topicNamesSet = topics.keySet();
        String[] topicNames = topicNamesSet.toArray(new String[topicNamesSet.size()]);
        Map<String, Set<String>> topicTalks = new HashMap<>();

        topicsiterator:
        for (String topicname : topicNames) {
            if (!topics.get(topicname).containsKey(wdwiki)) {
                System.out.println("There are is " + topicname + " in " + wdwiki);
                continue topicsiterator;
            }
            String topiccat = topics.get(topicname).get(wdwiki);
            System.out.println(wdwiki + "\t" + topiccat);
            String[] topicedtalks = w.getCategoryMembers(topiccat, 1);
            Set<String> talks = new HashSet<>();
            topicedtalksiterator:
            talks.addAll(Arrays.asList(topicedtalks));
            topicTalks.put(topicname, talks);
        } //END TOPICS

        //COUNTRIES
        Set<String> countryNamesSet = countries.keySet();
        String[] countryNames = countryNamesSet.toArray(new String[countryNamesSet.size()]);
        Map<String, Set<String>> countryTalks = new HashMap<>();

        countriesiterator:
        for (String countryname : countryNames) {
            if (!countries.get(countryname).containsKey(wdwiki)) {
                System.out.println("There are no " + countryname + "-articles in " + wdwiki);
                continue countriesiterator;
            }
            String countrycat = countries.get(countryname).get(wdwiki);
            System.out.println(wdwiki + "\t" + countrycat);
            String[] countriedtalks = w.getCategoryMembers(countrycat, 1);
            Set<String> talks = new HashSet<>();
            countriedtalksiterator:
            talks.addAll(Arrays.asList(countriedtalks));
            countryTalks.put(countryname, talks);
        } //END COUNTRIES

        Map<String, CEESUser> cashedUsers = new HashMap<>();
        Set<String> usersToFetch = new HashSet<>();

        //UTC
        Calendar contestStart = Config.contestStart;
        Calendar contestEnd = Config.contestEnd;
        // perfectly should be moved to some non-hardcoded configconfig
        // DST sucks.
        Config.Wiki wikiConfig = new Config.Wiki(wikiName);

        contestStart.add(Calendar.HOUR_OF_DAY, wikiConfig.timeZoneOffset);
        contestEnd.add(
                Calendar.HOUR_OF_DAY,
                (wikiConfig.usesDST ? wikiConfig.timeZoneOffset + 1 : wikiConfig.timeZoneOffset)
        );

        talkpageiteration:
        for (String talkPage : talkPages) {
            String article = talkPage.substring(talkPage.indexOf(":") + 1);
            Map pageInfo = w.getPageInfo(article);
            long pageidentifier = (long) pageInfo.get("pageid");
            int pageid = Integer.parseInt(pageidentifier + "");//page id which we need
            int size = (int) pageInfo.get("size");// page size in bytes which we need
            Wiki.Revision[] pageHistory = w.getPageHistory(article);
            if (pageHistory.length == 0) {
                continue talkpageiteration;
            }
            Wiki.Revision lastRevision = pageHistory[pageHistory.length - 1];
            Calendar timestamp = lastRevision.getTimestamp(); // page creation date which we need
            timestamp.setTimeZone(TimeZone.getTimeZone("UTC"));

            contestStart.setTimeZone(TimeZone.getTimeZone("UTC"));
            int startOffset = timestamp.compareTo(contestStart);
            int endoff = timestamp.compareTo(contestEnd);

            if (endoff > 0) {
                //the page was created after the contest end nothing to do here
                continue talkpageiteration;
            }
            int inWeek = getContestWeek(timestamp, contestStart, wikiConfig.usesDST);

            String isImproved = "no";

            if (startOffset < 0) {
                // the page was created before the contest start,
                // so it must've been improved
                isImproved = "yes";
            }

            System.out.println("Startoff = " + startOffset);
            boolean improverUnknown = false;
            String improver = "Misterious CEE Springman";

            if (startOffset < 0 && !templatePhobic) {
                improverUnknown = true;

                boolean pageTextsContainTalkPage = pageTexts.containsKey(talkPage);
                String talkPageText = "";
                if (pageTextsContainTalkPage) {
                    talkPageText = pageTexts.get(talkPage);
                    System.out.println("We've just got a\t" + wikiName + "\t" + talkPage + "\ttext from the batch variable.");
                } else {
                    System.out.println("Fetching " + wikiName + "\t" + talkPage + "\ttext from via API.");
                    talkPageText = w.getPageText(talkPage);
                }
                String regex = wikiConfig.templateRegex;

                if (!"".equals(regex) && talkPageText.matches(regex)) {
                    String parsedImprover = talkPageText.replaceAll(regex, "$2").trim().replaceAll("_", " ");
                    if (!"".equals(parsedImprover)) {
                        improver = parsedImprover;
                        improverUnknown = false;
                    }
                    System.out.println("Okay it's a " + wikiName + " improved article «" + article + "», the improver seem to be " + improver);
                } else {
                    System.out.println("No regex match for talk of\t" + article);
                }
            }

            // use oldest revision user for created articles, improver for improved
            String user = (startOffset < 0)
                    ? improver
                    : lastRevision.getUser().trim(); // username which we need

            if (user.matches("\\A(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)(\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)){3}\\z") || improverUnknown) {

                CEESUser ceesUser = new CEESUser(user);
                System.out.println("Talk pager:\t" + ceesUser + "\t" + improverUnknown);
                cashedUsers.put(user, ceesUser); // perfectly only anons show end up here
            } else if (cashedUsers.containsKey(user)) {

            } else {
                usersToFetch.add(user);
            }
            boolean pageTextsContainArticle = pageTexts.containsKey(article);
            String pt = "";
            if (pageTextsContainArticle) {
                pt = pageTexts.get(article);
                System.out.println("We've just got a\t" + wikiName + "\t" + article + "\ttext from the batch variable.");
            } else {
                System.out.println("Fetching " + wikiName + "\t" + article + "\ttext from via API.");
                pt = w.getPageText(article);
            }
            int words = countArticleWords(pt);
            int references = countArticleReferences(pt);
            int bytesSigma = 0;
            int weekSigma = 0;
            int weekOldid = 0;
            int authorOldid = 0;

            boolean hasValidRevisions = false;
            Set<String> revisionHashes = new HashSet<>();

            revisioniteration:
            for (int j = pageHistory.length - 1; j >= 0; j--) {
                Wiki.Revision revision = pageHistory[j];
                String revisionUser = revision.getUser();
                int sizeDiff = revision.getSizeDiff();
                //System.out.println("We are listing revisions of the article " + article + " now we are on #" + j + " and the diff is " + sizeDiff);
                Calendar revisionTime = revision.getTimestamp();
                //System.out.println("revuser\t" + revuser + "user\t" + user);
                String hashCode = revision.getSha1();
                boolean alreadypresentrev = revisionHashes.contains(hashCode);
                revisionHashes.add(hashCode);

                if (revisionUser == null) {
                    continue revisioniteration;
                }
                if (revisionUser.equals(user)
                        && revisionTime.compareTo(contestStart) >= 0
                        && !alreadypresentrev
                        && (!(revisionTime.compareTo(contestEnd) > 0))) {
                    hasValidRevisions = true;
                    bytesSigma += sizeDiff;
                    authorOldid = (int) revision.getRevid();

                }
            }

            if (!hasValidRevisions) {
                System.out.println(article + "\thas no valid revisions");
                continue talkpageiteration;
            }

            String wdentity = "N/A";
            String humanity = "N/A";
            String female = "N/A";
            String badge = "N/A";
            String[] WDEntityIsHuman = d.WDEntityIsHuman(article, wdwiki);
            if (WDEntityIsHuman != null) {
                wdentity = WDEntityIsHuman[0];
                humanity = WDEntityIsHuman[1];
                female = WDEntityIsHuman[2];
                switch (WDEntityIsHuman[3]) {
                    case "Q17437796":
                        badge = "FA";
                        break;
                    case "Q17437798":
                        badge = "GA";
                        break;
                    case "Q17506997":
                        badge = "FL";
                        break;
                    default:
                        badge = "";
                        break;
                }
            }

            boolean isfromthelist = false;
            synchronized (LOCK) {
                isfromthelist = theLists.contains(wdentity);
            }
            System.out.println(article + "\t" + weekOldid + "\tfrom lists:\t" + isfromthelist);

            String article_country = "";
            String article_topic = "";

            for (String topicname : topicNames) {
                if (topicTalks.containsKey(topicname)) {
                    if (topicTalks.get(topicname).contains(talkPage)) {
                        article_topic += ("".equals(article_topic) ? "" : ", ") + topicname;
                    }
                }
            }
            System.out.println(article + "\t" + article_topic);

            for (String countryname : countryNames) {
                if (countryTalks.containsKey(countryname)) {
                    if (countryTalks.get(countryname).contains(talkPage)) {
                        article_country += ("".equals(article_country) ? "" : ", ") + countryname;
                    }
                }
            }
            System.out.println(article + "\t" + article_country);

            synchronized (LOCK) {
                rows.add(new CEESTableLine(wikiName, pageid, article,
                        /*userid, caid,*/ user, /*gender,
        isnoob,*/ size, words, references,
                        article_country, article_topic,
                        humanity, female, timestamp.getTime(),
                        wdentity, isfromthelist, isImproved, inWeek, bytesSigma,
                        weekSigma, authorOldid, weekOldid, badge
                ));
            }
        } // end of going through talk pages

        String[] usersToFetchArray = usersToFetch.toArray(new String[usersToFetch.size()]);
        ReplicationFetcher cf = new ReplicationFetcher(c);
        //fetchUserData(String wikidb, String starttime, String noobtime, String... users)
        Map<String, CEESUser> usersData = cf.fetchUsersData(
                3366 + 1 + thread, wdwiki,
                "20170321000000",// hardcode
                "20170121000000",// hardcode
                usersToFetchArray
        );
        cashedUsers.putAll(usersData);

        boolean isLastThread = false;
        synchronized (LOCK) {
            wikisusers.put(wikiName, cashedUsers);
            maincounter++;
            isLastThread = maincounter == maincounterlim;
            System.out.println(maincounter + "\t" + wikiName);
        }
        if (isLastThread) {
            writeToDatabase();
        }
    }//end of work in wiki

    /**
     * Writes all the stuff we fetched to our localhost database so that we can
     * proceed data later for the outputs. Reads all the stuff off global
     * variables, thus no input params are used.
     *
     * @throws Exception
     */
    public static void writeToDatabase() throws Exception {
        CEESTableLine[] rowses = rows.toArray(new CEESTableLine[rows.size()]);
        System.out.println("There are " + rowses.length + " rows in total");
        DatabasePartVII db = new DatabasePartVII();
        db.truncateTable("2017");
        for (int i = 0; i < rowses.length; i++) {
            CEESTableLine row = rowses[i];
            System.out.println(i + "\t-\tWriting to DB");
            Map<String, CEESUser> wikiusers = wikisusers.get(row.wname);
            System.out.println(row.article + "\t" + row.user);

            CEESUser wikiuser = wikiusers.containsKey(row.user) ? wikiusers.get(row.user) : new CEESUser(row.user);
            System.out.println(wikiuser.toString());
            db.Inserterie(row, wikiuser);
        }
    }

    /**
     * Resolves redirects for Wikibase repository (Wikidata) entities
     *
     * @param d - BaseBot instance for Wikibase repository (Wikidata)
     * @throws IOException
     */
    public static void resolveEntityRedirects(BaseBot d) throws IOException {
        String[] qses = qs.toArray(new String[qs.size()]);
        qs.clear();
        String query = "";
        for (String string : qses) {
            query += "".equals(query) ? string : "|" + string;
        }
        String[] qids = d.WDEntity(query.split("\\|"));
        theLists.addAll(Arrays.asList(qids));
        //System.out.println(j + "\t" + string);
    }

    /**
     * Fetches texts for all pages connected to page talks given by batches of
     * limit given or the rest
     *
     * @param lim - limitation of number of articles texts to be fetched of per
     * batch
     * @param w - BaseBot instance to work with
     * @param talks - list of page talks to fetch connected pages texts of
     * @return A map with keys for page titles and values for page texts
     * @throws Exception
     *
     * @todo Investigate why this method doesn't return <em>all</em> the page
     * texts
     */
    public static Map<String, String> fetchPageTexts(int lim, BaseBot w, String... talks) throws Exception {
        String batchurlpart = "";
        Map<String, String> pageTexts = new HashMap<>();
        int j = 0;
        for (int i = 0; i < talks.length; i++) {
            String talk = talks[i];
            String article = talk.substring(talk.indexOf(":") + 1);
            batchurlpart += "".equals(batchurlpart) ? article : "|" + article;
            if (++j == lim || i == talks.length - 1 /*|| URLEncoder.encode(batchurlpart, "UTF-8").length() > 600*/) {
                pageTexts.putAll(w.batchPageTexts(batchurlpart));
                batchurlpart = "";
            }
        }
        j = 0;
        for (int i = 0; i < talks.length; i++) {
            String talk = talks[i];
            batchurlpart += "".equals(batchurlpart) ? talk : "|" + talk;
            if (++j == lim || i == talks.length - 1 || URLEncoder.encode(batchurlpart, "UTF-8").length() > 600) {
                pageTexts.putAll(w.batchPageTexts(batchurlpart));
                batchurlpart = "";
            }
        }
        return pageTexts;
    }

    static int countArticleWords(String pageText) {
        return pageText.split("[\\p{Po}\\p{Z}\\p{S}\\p{C}]+").length;
    }

    static int countArticleReferences(String pageText) {
        return pageText.split("<ref\\s").length - 1;
    }

    static int getContestWeek(Calendar revisionTime, Calendar startTime, boolean usesDST) {
        int inWeek;
        long revisionMillis = revisionTime.getTimeInMillis();
        long startMillis = revisionTime.getTimeInMillis();
        long hourTime = 3600 * 1000;
        long weekTime = 7 * 24 * hourTime;
        long diff = revisionMillis - startMillis;
        if (diff < weekTime) {//simple case independent of DST
            inWeek = 1;
        } else {
            if (usesDST) {
                //homogenizing week lengths for counties with DST in place
                startMillis += hourTime;
                diff = revisionMillis - startMillis;
            }
            inWeek = (int)Math.ceil(diff/weekTime);
        }
        return inWeek;
    }

    static void fetchListedArticlesEntities(BaseBot m, BaseBot d) throws IOException {
        String[] lists = m.listPages("Wikimedia CEE Spring 2017/Structure/", null, 0);
        for (String list : lists) {
            System.out.println(list);
            if ("Wikimedia CEE Spring 2017/Structure/Statistics".equals(list)) {
                continue;
            }

            String listText = m.getPageText(list);
            boolean cont = true;
            qs.clear();

            do {
                int a = listText.indexOf("{{#invoke:WikimediaCEETable");
                if (a > -1) {
                    int b = listText.indexOf("}}", a);
                    String tl = listText.substring(a, b);
                    listText = listText.substring(b);
                    String[] split = tl.split("\\|Q");

                    for (int j = 1; j < split.length; j++) {
                        String q = split[j];
                        q = "Q" + q.replaceAll("[^\\d]+", "");
                        qs.add(q);
                        if (qs.size() >= 400) {
                            resolveEntityRedirects(d);
                        }
                    }
                } else {
                    cont = false;
                    resolveEntityRedirects(d);
                }
            } while (cont);
        }
    }

    static Map<String, String> fetchTemplateUsingWikis(BaseBot d) throws IOException {
        return d.getSiteLinks("Q28927040");
    }

}//Class end
