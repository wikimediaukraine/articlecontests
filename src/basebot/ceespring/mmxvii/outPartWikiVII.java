/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basebot.ceespring.mmxvii;

import basebot.ceespring.out.*;
import basebot.ceespring.DatabasePart;
import java.sql.ResultSet;
import org.wikipedia.BaseBot;

/**
 *
 * @author Base <base-w at yandex.ru>
 */
public class outPartWikiVII {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        BaseBot m = new BaseBot("meta.wikimedia.org");
        m.setUserAgent("BaseBot");
        m.login(args[0], args[1]);
        m.setMarkBot(true);
        m.setMarkMinor(true);

        DatabasePart db = new DatabasePart();

        String wrt = "Raw output of data gathered. Only wikis with the template and cswwere taken into account for now.\n";
        wrt += "As of the newest article in [[User:BaseBot/CEES/MMXVI/General output|General output]] :)\n\n";
        wrt += "== Table ==\n";

        wrt += "{{User:BaseBot/CEES/MMXVII/Wikis/header}}\n";

        String query = ""
                + "SELECT `wiki` as w,\n"
                + " COUNT(`article_id`) AS articles,\n"
                + " SUM(CASE WHEN  `article_isimproved` =  \"no\" THEN 1 ELSE 0 END) as created_articles,\n"
                + " SUM(CASE WHEN  `article_isimproved` =  \"yes\" THEN 1 ELSE 0 END) as improved_articles,\n"
                + " SUM(CASE WHEN  `article_fromlist` =  \"1\" THEN 1 ELSE 0 END) as listed_articles,\n"
                + " SUM(CASE WHEN  `article_isperson` =  \"yes\" THEN 1 ELSE 0 END) as about_people,\n"
                + " SUM(CASE WHEN  `article_iswoman` =  \"yes\" THEN 1 ELSE 0 END) as about_women,\n"
                + " COUNT(DISTINCT `author_name`) AS authors,\n"
                + " (SELECT COUNT(DISTINCT `author_name`) FROM `2016` WHERE `author_gender` = 'female' AND `wiki` = w ) AS female_authors,\n"
                + " (SELECT COUNT(DISTINCT `author_name`) FROM `2016` WHERE `author_isnoob` = \"no\" AND `wiki` = w ) AS `non-noobs`,\n"
                + " (SELECT COUNT(DISTINCT `author_name`) FROM `2016` WHERE `author_isnoob` = \"yes\" AND `wiki` = w ) AS noobs\n"
                + "FROM `2017` \n"
                + "WHERE 1\n"
                + "GROUP BY `wiki`\n"
                + "ORDER BY articles DESC";
        System.out.println(query);

        ResultSet generalOutputtie = db.generalOutputtie(query);

        String graph_lang = "";
        String graph_articles = "";

        String graph_created = "";
        String graph_improved = "";

        while (generalOutputtie.next()) {
            wrt += "{{User:BaseBot/CEES/MMXVII/Wikis/row";
            wrt += "\n|wiki            = " + generalOutputtie.getString(1);
            wrt += "\n|articles        = " + generalOutputtie.getInt(2);
            wrt += "\n|created         = " + generalOutputtie.getInt(3);
            wrt += "\n|improved        = " + generalOutputtie.getInt(4);
            wrt += "\n|listed          = " + generalOutputtie.getInt(5);
            wrt += "\n|about_people    = " + generalOutputtie.getInt(6);
            wrt += "\n|about_women     = " + generalOutputtie.getInt(7);
            wrt += "\n|authors         = " + generalOutputtie.getInt(8);
            wrt += "\n|female_authors  = " + generalOutputtie.getInt(9);
            wrt += "\n|not_new         = " + generalOutputtie.getInt(10);
            wrt += "\n|new_users       = " + generalOutputtie.getInt(11);
            wrt += "\n}}\n\n";

            graph_lang += graph_lang.equals("") ? "" : ", ";
            graph_lang += generalOutputtie.getString(1).substring(0, generalOutputtie.getString(1).indexOf('.'));

            graph_articles += graph_articles.equals("") ? "" : ", ";
            graph_articles += generalOutputtie.getInt(2);

            graph_created += graph_created.equals("") ? "" : ", ";
            graph_created += generalOutputtie.getInt(3);

            graph_improved += graph_improved.equals("") ? "" : ", ";
            graph_improved += generalOutputtie.getInt(4);
        }

        String query2
                = "SELECT \n"
                + " \"total\",\n"
                + " COUNT(`article_id`) AS articles,      \n"
                + " SUM(CASE WHEN  `article_isimproved` =  \"no\" THEN 1 ELSE 0 END) as created_articles,\n"
                + " SUM(CASE WHEN  `article_isimproved` =  \"yes\" THEN 1 ELSE 0 END) as improved_articles,\n"
                + " SUM(CASE WHEN  `article_fromlist` =  \"1\" THEN 1 ELSE 0 END) as listed_articles,         \n"
                + " SUM(CASE WHEN  `article_isperson` =  \"yes\" THEN 1 ELSE 0 END) as about_people,\n"
                + " SUM(CASE WHEN  `article_iswoman` =  \"yes\" THEN 1 ELSE 0 END) as about_women,\n"
                + " COUNT(DISTINCT `author_name`) AS authors,\n"
                + " (SELECT COUNT(DISTINCT `author_name`) FROM `2016` WHERE `author_gender` = 'female') AS female_authors,\n"
                + " (SELECT COUNT(DISTINCT `author_name`) FROM `2016` WHERE `author_isnoob` = \"no\") AS `non-noobs`,\n"
                + " (SELECT COUNT(DISTINCT `author_name`) FROM `2016` WHERE `author_isnoob` = \"yes\") AS noobs\n"
                + "FROM `2017` \n"
                + "WHERE 1\n"
                + "ORDER BY articles DESC";

        ResultSet totalOuputtie = db.generalOutputtie(query2);

        while (totalOuputtie.next()) {
            wrt += "{{User:BaseBot/CEES/MMXVII/Wikis/row";
            wrt += "\n|wiki            = " + "&Sigma;";
            wrt += "\n|articles        = " + totalOuputtie.getInt(2);
            wrt += "\n|created         = " + totalOuputtie.getInt(3);
            wrt += "\n|improved        = " + totalOuputtie.getInt(4);
            wrt += "\n|listed          = " + totalOuputtie.getInt(5);
            wrt += "\n|about_people    = " + totalOuputtie.getInt(6);
            wrt += "\n|about_women     = " + totalOuputtie.getInt(7);
            wrt += "\n|authors         = " + totalOuputtie.getInt(8);
            wrt += "\n|female_authors  = " + totalOuputtie.getInt(9);
            wrt += "\n|not_new         = " + totalOuputtie.getInt(10);
            wrt += "\n|new_users       = " + totalOuputtie.getInt(11);
            wrt += "\n}}\n\n";
        }

        wrt += "\n|}\n\n";

        String graph = "{{User:BaseBot/CEES/MMXVII/Wikis/graph\n";
        graph += "|language  = " + graph_lang + "\n";
        graph += "|created   = " + graph_created + "\n";
        graph += "|improved  = " + graph_improved + "\n";
        graph += "}}";

        String pie = "{{User:BaseBot/CEES/MMXVII/Wikis/pie\n";
        pie += "|language   = " + graph_lang + "\n";
        pie += "|articles   = " + graph_articles + "\n";
        pie += "}}";

        wrt += "== Graph ==\n";

        wrt += "\n=== Bars ===\n";
        wrt += "\n\n" + graph + "\n\n";

        wrt += "\n=== Pie  ===\n";
        wrt += "\n\n" + pie + "\n\n";

        m.edit("User:BaseBot/CEES/MMXVII/Wikis", wrt, "creating/updating");
        System.out.println(wrt);

        db.stopDB();
    }

}
