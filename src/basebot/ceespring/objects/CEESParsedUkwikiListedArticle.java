package basebot.ceespring.objects;

/**
 * @author Base <base-w at yandex.ru>
 */
public class CEESParsedUkwikiListedArticle {

    //I use talks and not just articles just so that the code is the same for the case
    public String id;
    public String isalist;
    public String disqualified;
    public String notes;
    public String Krutyvuss;
    public String Volodymyr_D_k;
    public String Visem;
    public String JurorYon;
    public String highlight;

    public CEESParsedUkwikiListedArticle(
            String id_p,
            String isalist_p,
            String disqualified_p,
            String notes_p,
            String Krutyvuss_p,
            String Volodymyr_D_k_p,
            String Visem_p,
            String JurorYon_p,
            String highlight_p) {
        this.id = id_p;
        this.isalist = isalist_p;
        this.disqualified = disqualified_p;
        this.notes = notes_p;
        this.Krutyvuss = Krutyvuss_p;
        this.Volodymyr_D_k = Volodymyr_D_k_p;
        this.Visem = Visem_p;
        this.JurorYon = JurorYon_p;
        this.highlight = highlight_p;
    }
;
}
