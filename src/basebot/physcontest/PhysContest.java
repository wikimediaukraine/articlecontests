/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basebot.physcontest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import objects.PhysContestArticle;
import org.wikipedia.BaseBot;
import org.wikipedia.Wiki;

/**
 *
 * @author Base <base-w at yandex.ru>
 */
public class PhysContest {

    /**
     * @param args the command line arguments
     * @throws java.lang.Exception
     */
    public static void main(String[] args) throws Exception {
        BaseBot w = new BaseBot("uk.wikipedia.org");
        w.setUserAgent("BaseBot");
        w.login(args[0], args[1]);
        w.setMarkBot(true);
        w.setMarkMinor(true);

        String wrt = "List of [[wmua:WikiPhysContest-2016|]] participants\n\n\n";

        String[] cat = w.whatTranscludesHere("Шаблон:WikiPhysContest-2016", 1);

        Set<PhysContestArticle> articleset = new HashSet<>();

        for (String talk : cat) {
            String article = talk.substring(talk.indexOf(':') + 1);
            Map articleInfo = w.getPageInfo(article);
            String id = articleInfo.get("pageid").toString();
            Calendar starttime = new GregorianCalendar();
            starttime.set(2016, 2, 31, 20, 59, 59);
            Wiki.Revision[] revsbeforestart = w.getPageHistory(article, null, starttime, false);
            int initialsize = revsbeforestart.length > 0 ? revsbeforestart[0].getSize() : 0;

            Calendar endtime = new GregorianCalendar();
            endtime.set(2016, 4, 14, 20, 59, 59);
            Wiki.Revision[] revsbeforeend = w.getPageHistory(article, null, endtime, false);
            int size = revsbeforeend.length > 0 ? revsbeforeend[0].getSize() : 0;

            String tpt = w.getPageText(talk);
            //w.edit(talk, tpt, "purge");
            String regex = "(?s).*\\{\\{\\s*[Ww]ikiPhysContest-2016\\s*\\|[^}]*користувач\\s*=([^\\|}]+)[^}]*}}.*";
            String user = "";
            if (tpt.matches(regex)) {
                user = tpt.replaceAll(regex, "$1").trim();
            } else {
                System.out.println("On\t" + talk + "\tno user is mentioned");
                continue;
            }

            regex = "(?s).*\\{\\{\\s*[Ww]ikiPhysContest-2016\\s*\\|[^}]*група\\s*=([^\\|}]+)[^}]*}}.*";
            String category = "";
            if (tpt.matches(regex)) {
                category = tpt.replaceAll(regex, "$1").trim();
            } else {
                System.out.println("On\t" + talk + "\tno category is mentioned");
                continue;
            }
            boolean userExists = w.userExists(user);
            if (userExists) {
                String userid = (String) w.getUser(user).getUserInfo().get("userid");
                articleset.add(new PhysContestArticle(article, id, user, userid, size, initialsize, category));
            } else {
                System.out.println("User\t" + user + "\t, mentioned on\t" + talk + "\tdoes not actually exit.");
            }
        }
        PhysContestArticle[] articles = articleset.toArray(new PhysContestArticle[articleset.size()]);
        databaseit(articles);
        System.out.println(wrt);
    }

    /**
     *
     * @param pcas
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public static void databaseit(PhysContestArticle[] pcas) throws SQLException, ClassNotFoundException {
        Connection conn;
        String myDriver = "org.gjt.mm.mysql.Driver";
        String myUrl = "jdbc:mysql://localhost/physcontest?characterEncoding=utf8";
        Class.forName(myDriver);
        conn = DriverManager.getConnection(myUrl, "root", "");
        String query = "TRUNCATE `" + 2016 + "`";
        Statement st = conn.createStatement();
        st.execute(query);
        System.out.println("We are going to database\t" + pcas.length + "\tarticles");
        for (int i = 0; i < pcas.length; i++) {
            System.out.println("Databasing article\t#" + i);
            PhysContestArticle pca = pcas[i];
            query = " insert into `2016` ("
                    + " article_name,"
                    + " article_id,"
                    + " user_name,"
                    + " user_id,"
                    + " article_size,"
                    + " article_initial_size,"
                    + " article_category"
                    + ")"
                    + " values (?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1, pca.articlename);
            preparedStmt.setString(2, pca.curid);
            preparedStmt.setString(3, pca.user);
            preparedStmt.setString(4, pca.userid);
            preparedStmt.setInt(5, pca.size);
            preparedStmt.setInt(6, pca.initialsize);
            preparedStmt.setString(7, pca.category);
            preparedStmt.execute();
        }
        conn.close();
    }
}
