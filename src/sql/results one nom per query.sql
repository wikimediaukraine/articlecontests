SELECT
    CONCAT( "|-\n|  || ", `article_author` , " || " ),
     cast( sum(case when `article_week` = 1 then `article_mark` else 0 end) AS decimal( 10, 1 ) ) AS week1
FROM `2016-ukwiki-results`
WHERE 1
GROUP BY `article_author`
ORDER BY week1 desc
